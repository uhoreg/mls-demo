// webcrypto polyfill
import { Crypto } from "@peculiar/webcrypto";
global.crypto = new Crypto();

import { Client, IClientOpts, MLS_ALGORITHM } from "./client";
import * as tlspl from "mls/lib/tlspl";
import {MLSCiphertext} from "mls/lib/message";
import {Process, base64Decode, sleep} from "./util";
import { Server } from "socket.io";

const io = new Server({
    serveClient: false,
    cors: {
        origin: "*",
        methods: ["GET", "POST"],
    },
});

const letters = "ZABCDEFGHIJKLMNOPQRSTUVWXYZ";
function makeShortname(n: number): string {
    n++;
    let output = "";
    for (;;) {
        if (n < 27) {
            return letters[n] + output;
        } else {
            n--;
            const rem = n % 26;
            n = (n - rem) / 26;
            output = letters[rem + 1] + output;
        }
    }
}

interface IDeviceOpts extends IClientOpts {
    noAutoSync?: boolean;
}

class Device {
    private readonly process: Process;
    private readonly client: Client;
    private readonly sent: Map<string, any>;
    private readonly logInPromise: Promise<any>;
    constructor(
        homeserverUrl: string,
        username: string, password: string,
        private readonly socket: any,
        private readonly shortnameMap: Record<string, Record<string, string>>,
        public readonly shortname: string,
        private readonly senders: Record<string, string>,
        private readonly opts: IDeviceOpts = {},
    ) {
        this.process = new Process(this.messageHandler, this);
        this.client = new Client(homeserverUrl, opts);
        this.sent = new Map();
        this.logInPromise = this.process.send({ type: "login", username, password });
    }

    disconnect(): Promise<void> {
        return this.process.send({ type: "disconnect" })
    }

    async syncHandler(messages: any[], toDevice: any[]) {
        for (let i = 0; i < messages.length; i++) {
            const e = messages[i];
            if (e.type === "m.room.encrypted") {
                if (e.unsigned && e.unsigned.transaction_id) {
                    e._decrypted = this.sent.get(e.unsigned.transaction_id);
                } else {
                    // eslint-disable-next-line comma-dangle,array-bracket-spacing
                    const [[decodedMlsCiphertext], ] = tlspl.decode(
                        [MLSCiphertext.decode], base64Decode(e.content.ciphertext),
                    );
                    /*console.log(
                      this.client.userId, this.client.deviceId,
                      "decrypting", e.event_id,
                      `${decodedMlsCiphertext.epoch}, ${e.content.epoch_sender[0]}, ${e.content.epoch_sender[1]}`,
                      e.content.resolves,
                      );*/
                    try {
                        const decrypted = await this.client.decryptMessage(e);
                        if (!decrypted) {
                            const sender = this.senders[e.event_id];
                            if (sender) {
                                this.socket.emit("failedDecryption", {
                                    user: this.shortname,
                                    epoch: `${decodedMlsCiphertext.epoch + 1}|${sender}`,
                                });
                            }
                        } else if (decrypted.type === "[MLS Commit]") {
                            const resolved = e.content.resolves.map(([epoch, [user, device]]) => `${epoch}|${this.shortnameMap[user][device]}`);
                            resolved.push(`${decodedMlsCiphertext.epoch}|${this.shortnameMap[e.content.epoch_sender[0]][e.content.epoch_sender[1]]}`);
                            this.socket.emit("decrypted", {
                                user: this.shortname,
                                epoch: `${decodedMlsCiphertext.epoch + 1}|${this.shortnameMap[e._sender[0]][e._sender[1]]}`,
                                resolved,
                            });
                        }
                        e._decrypted = decrypted;
                    } catch (err) {
                        const sender = this.senders[e.event_id];
                        if (sender) {
                            this.socket.emit("failedDecryption", {
                                user: this.shortname,
                                epoch: `${decodedMlsCiphertext.epoch + 1}|${sender}`,
                            });
                        }
                        console.log(this.client.userId, this.client.deviceId, "error", e.event_id);
                        console.log(err);
                        console.log(e.sender, e.content.epoch_sender, e.content.resolves);
                    }
                }
            }
        }
        this.process.send({ type: "sync", messages, toDevice });
    }

    sentEventHandler(event, decrypted) {
        if (decrypted.type === "[MLS Commit]") {
            // eslint-disable-next-line comma-dangle,array-bracket-spacing
            const [[decodedMlsCiphertext], ] = tlspl.decode(
                [MLSCiphertext.decode], base64Decode(event.content.ciphertext),
            );
            const resolves = event.content.resolves.map(
                ([epoch, [user, device]]) => `${epoch}|${this.shortnameMap[user][device]}`,
            );
            const base = `${decodedMlsCiphertext.epoch}|${this.shortnameMap[event.content.epoch_sender[0]][event.content.epoch_sender[1]]}`;
            this.socket.emit("epoch", {
                id: `${decodedMlsCiphertext.epoch + 1}|${this.shortname}`,
                base, resolves,
            });
        }
    }

    async messageHandler(message: any): Promise<void> {
        switch (message.type) {
            case "login":
                await this.client.login(message.username, message.password);
                console.log(`${this.client.userId} ${this.client.deviceId} -> ${this.shortname}`);
                this.shortnameMap[this.client.userId][this.client.deviceId] = this.shortname;
                if (!this.opts.noAutoSync) {
                    this.client.startSync(this.syncHandler.bind(this), this.sentEventHandler.bind(this));
                }
                break;

            case "startSync":
                this.client.startSync(this.syncHandler.bind(this), this.sentEventHandler.bind(this));
                break;

            case "disconnect":
                this.process.kill();
                this.client.logout();
                break;

            case "updateTree": {
                const eventId = await this.client.updateTree(message.force);
                this.senders[eventId] = this.shortname;
                break;
            }

            case "createRoom":
                this.socket.emit("epoch", {id: `1|${this.shortname}`});
                await this.client.createRoom({
                    preset: "private_chat",
                    is_direct: true,
                    invite: message.invitees,
                    initial_state: [
                        {
                            type: "m.room.encryption",
                            state_key: "",
                            content: {
                                algorithm: MLS_ALGORITHM,
                            },
                        },
                    ],
                });
                break;

            case "leave":
                await this.client.leave();
                break;

            case "waitForGroup":
                await new Promise(resolve => {
                    this.client.waitForGroup(resolve);
                });
                break;

            case "sleep":
                await new Promise(resolve => {
                    setTimeout(resolve, message.delay || 500);
                });
                break;
        }
    }

    waitForLogin(): Promise<any> {
        return this.logInPromise;
    }

    updateTree(force: boolean): Promise<void> {
        return this.process.send({ type: "updateTree", force });
    }

    createRoom(invitees: string[]): Promise<void> {
        return this.process.send({ type: "createRoom", invitees });
    }

    leave(): Promise<void> {
        return this.process.send({ type: "leave" });
    }

    waitForGroup(): Promise<void> {
        return this.process.send({ type: "waitForGroup" });
    }

    sleep(delay: number): Promise<void> {
        return this.process.send({ type: "sleep", delay});
    }

    startSync(): Promise<void> {
        return this.process.send({ type: "startSync" });
    }
}

function select<T>(collection: Iterable<T>, elem: number): T {
    let i = 0;
    const iter = collection[Symbol.iterator]();
    while (true) {
        const iterVal = iter.next();
        if (iterVal.done) {
            throw new Error("Unknown element");
        } else if (++i > elem) {
            return iterVal.value;
        }
    }
}

io.on("connection", async (socket) => {
    console.log("got connection");
    const devices: Device[] = [];
    let backfillDevice: Device;
    try {
        let deviceNum = 0;

        let num = 0;

        const availableDevices: Set<number> = new Set();

        // if we can't decrypt a Commit, then we don't know who sent it, and we
        // don't know what epoch it created.  So todisplay that we can't decrypt
        // that epoch, we remember globally which device sent which event ID so
        // that we can look it up later.
        const senders: Record<string, string> = {};

        let numUpdates = 0;

        const doUpdate = function() {
            /*
            if (Math.random() < 0.01) {
                socket.emit("joined", { user: makeShortname(deviceNum) });
                const newDevice = Math.random() < 0.5 ?
                    new Device(
                        "http://localhost:8008", "alice", "secret",
                        socket,
                        shortnameMap, makeShortname(deviceNum++), senders,
                        { sendDelay: 2000 },
                    ) :
                    new Device(
                        "http://localhost:8008", "bob", "secret",
                        socket,
                        shortnameMap, makeShortname(deviceNum++), senders,
                        { sendDelay: 1000 },
                    );
                newDevice.waitForGroup()
                    .then(() => {
                        availableDevices.add(devices.length);
                        devices.push(newDevice);
                    });
                return;
            }
            */
            if (availableDevices.size === 0 || Math.random() > 0.5 * (1 - num / availableDevices.size)) {
                return;
            }
            numUpdates++;
            const selectedDeviceNum = Math.floor(Math.random() * availableDevices.size);
            const deviceIdx = select(availableDevices, selectedDeviceNum);
            availableDevices.delete(deviceIdx);
            const device = devices[deviceIdx];
            /*
            if (deviceIdx !== 0 && deviceIdx !== 1 && Math.random() < 0.1) {
                socket.emit("left", { user: device.shortname });
                device.disconnect();
                return;
            }
            */
            num++;
            device.updateTree(true)
                .then(() => {
                    num--;
                    availableDevices.add(deviceIdx);
                });
        }

        const shortnameMap: Record<string, Record<string, string>> = {
            "@alice:mls": {},
            "@bob:mls": {},
        };

        backfillDevice = new Device(
            "http://localhost:8008", "alice", "secret",
            socket,
            shortnameMap, makeShortname(deviceNum++), senders,
            { sendDelay: 2000, noAutoSync: true },
        );

        for (let i = 0; i < 6; i++) {
            devices.push(new Device(
                "http://localhost:8008", "alice", "secret",
                socket,
                shortnameMap, makeShortname(deviceNum++), senders,
                { sendDelay: 2000 },
            ));
            devices.push(new Device(
                "http://localhost:8008", "bob", "secret",
                socket,
                shortnameMap, makeShortname(deviceNum++), senders,
                { sendDelay: 1000 },
            ));
        }
        for (const device of devices) {
            await device.waitForLogin();
        }
        devices[0].createRoom(["@bob:mls"]);
        for (let i = 0; i < devices.length; i++) {
            devices[i].waitForGroup();
            availableDevices.add(i)
        }


        while (numUpdates < 50) {
            doUpdate();
            await sleep(100);
        }

        for (let i = 0; i < devices.length; i++) {
            await devices[i].sleep(0);
        }

        await sleep(1000);

        backfillDevice.startSync();

        await sleep(4000);

        console.log("done");
    } finally {
        socket.disconnect();
        if (devices[0]) {
            devices[0].leave();
        }
        if (devices[1]) {
            devices[1].leave();
        }
        await Promise.all(devices.map(device => device.disconnect()));
        if (backfillDevice) {
            backfillDevice.disconnect();
        }
    }
});

io.listen(3000);
