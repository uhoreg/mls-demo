// eslint doesn't seem to recognize when imported names are used for type names
// and complains about unused variables, so we disable the error on those lines
import {mls10_128_DhKemX25519Aes128GcmSha256Ed25519 as cipherSuite} from "mls/lib/ciphersuite";
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import {KEMPrivateKey} from "mls/lib/hpke/base";
import {KeyPackage} from "mls/lib/keypackage";
import {
    SignatureScheme,
    ProtocolVersion,
} from "mls/lib/constants";
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import {Credential, BasicCredential} from "mls/lib/credential";
import {stringToUint8Array} from "mls/lib/util";
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import {Ed25519, SigningPrivateKey} from "mls/lib/signatures";
import {
    MLSCiphertext,
    MLSPlaintext, // eslint-disable-line @typescript-eslint/no-unused-vars
    Add,
    Update,
    Remove,
    Commit,
    ProposalWrapper,
    Reference,
} from "mls/lib/message";
import {Welcome} from "mls/lib/welcome";
import {Buffer} from "buffer";
import * as tlspl from "mls/lib/tlspl";
import {Group} from "mls/lib/group";
import {base64Decode, base64Encode, uint8ArrayToHex, uint8ArrayToString} from "./util";
import fetch from "cross-fetch";

export const MLS_ALGORITHM =
    "org.matrix.msc2883.v0.mls.dhkemx25519-aes128gcm-sha256-ed25519";
const MLS_INIT_KEY = "org.matrix.msc2883.v0.mls.init_key";

export type SentEventHandler = (event: Record<string, any>, decrypted: any, txnid: string) => any;
export type SyncHandler = (msgs: any[], toDevice: any[]) => Promise<void>;

function makeIdentity(userId: string, deviceId: string): Uint8Array {
    return stringToUint8Array(userId + "|" + deviceId);
}

export interface IClientOpts {
    sendDelay?: number;
}

export class Client {
    private accessToken?: string;
    public deviceId?: string;
    public userId?: string;
    private ed25519?: SigningPrivateKey;
    private ed25519ser?: Uint8Array;
    private oneTimeKeys: Record<string, [KeyPackage, KEMPrivateKey]>;
    private roomId: string | undefined;
    private sync: boolean;
    private since: string | undefined;
    private credential?: Credential;
    public group?: Group;
    private members?: Set<string>;
    private usersNeedingUpdate: Set<string>;
    private userDevicesInRoom: Map<string, Set<string>>;
    public sentEventHandler: SentEventHandler;
    private txnId: number;
    private groupWaiters: (() => any)[];
    private gotSync = false;
    private epochMap: Map<string, string>;
    private pending: Map<string, Promise<any>>;
    constructor(readonly homeserverUrl: string, private readonly opts: IClientOpts = {}) {
        this.oneTimeKeys = {};
        this.sync = false;
        this.usersNeedingUpdate = new Set();
        this.txnId = 0;
        this.groupWaiters = [];
        this.epochMap = new Map();
        this.pending = new Map();
    }

    private async post(path: string, data: any = {}): Promise<any> {
        const headers: Record<string, string> = {
            "Content-Type": "application/json",
        };
        if (this.accessToken) {
            headers.Authorization = `Bearer ${this.accessToken}`;
        }
        const response = await fetch(
            `${this.homeserverUrl}/_matrix/client/${path}`,
            {
                method: "POST",
                headers: headers,
                body: JSON.stringify(data),
            },
        );
        if (response.status > 400) {
            throw await response.json();
        } else {
            return response.json();
        }
    }
    private async put(path: string, data: any = {}): Promise<any> {
        const headers: {[header: string]: string} = {
            "Content-Type": "application/json",
        };
        if (this.accessToken) {
            headers.Authorization = `Bearer ${this.accessToken}`;
        }
        const response = await fetch(
            `${this.homeserverUrl}/_matrix/client/${path}`,
            {
                method: "PUT",
                headers: headers,
                body: JSON.stringify(data),
            },
        );
        if (response.status > 400) {
            throw await response.json();
        } else {
            return response.json();
        }
    }
    private async get(path: string): Promise<any> {
        const headers: {[header: string]: string} = {
            "Content-Type": "application/json",
        };
        if (this.accessToken) {
            headers.Authorization = `Bearer ${this.accessToken}`;
        }
        const response = await fetch(
            `${this.homeserverUrl}/_matrix/client/${path}`,
            {
                method: "GET",
                headers: headers,
            },
        );
        if (response.status > 400) {
            throw await response.json();
        } else {
            return response.json();
        }
    }

    async login(username: string, password: string): Promise<void> {
        const response = await this.post("r0/login", {
            type: "m.login.password",
            identifier: {
                type: "m.id.user",
                user: username,
            },
            password: password,
        });
        this.accessToken = response.access_token;
        this.deviceId = response.device_id;
        this.userId = response.user_id;
        this.usersNeedingUpdate.add(this.userId);

        // create and upload keys
        const [privSignKey, pubSignKey] =
            await Ed25519.generateKeyPair();
        this.ed25519 = privSignKey;

        this.ed25519ser = await pubSignKey.serialize();
        const ed25519b64 = base64Encode(this.ed25519ser);

        const deviceKeys = {
            user_id: this.userId,
            device_id: this.deviceId,
            algorithms: [MLS_ALGORITHM],
            keys: {
                [`ed25519:${this.deviceId}`]: ed25519b64,
            },
        };

        this.credential = new BasicCredential(
            makeIdentity(this.userId, this.deviceId),
            SignatureScheme.ed25519,
            this.ed25519ser,
        );
        const oneTimeKeyPromises: Promise<[KEMPrivateKey, Uint8Array, KeyPackage]>[] = [];
        for (let i = 0; i < 50; i++) {
            oneTimeKeyPromises.push(
                cipherSuite.hpke.kem.generateKeyPair()
                    .then(async ([privKey, pubKey]) => {
                        const ser = await pubKey.serialize();
                        const keyPackage = await KeyPackage.create(
                            ProtocolVersion.Mls10,
                            cipherSuite,
                            ser,
                            this.credential,
                            [],
                            privSignKey,
                        );
                        return [privKey, ser, keyPackage];
                    }),
            );
        }
        const oneTimeKeys: [KEMPrivateKey, Uint8Array, KeyPackage][] =
            await Promise.all(oneTimeKeyPromises);
        const otks: {[id: string]: any} = {};
        for (const [privKey, pubKey, keyPackage] of oneTimeKeys) {
            const pubKeyB64 = base64Encode(pubKey);
            const keyPackageB64 = base64Encode(tlspl.encode([keyPackage.encoder]));
            // FIXME: add validity
            otks[`${MLS_INIT_KEY}:${pubKeyB64}`] = {
                key: keyPackageB64,
            };
            this.oneTimeKeys[pubKeyB64] = [keyPackage, privKey];
        }
        await this.post("r0/keys/upload", {
            device_keys: deviceKeys,
            one_time_keys: otks,
        });
    }
    async logout(): Promise<void> {
        this.sync = false;
        await this.post("r0/logout");
        this.accessToken = undefined;
        this.deviceId = undefined;
        this.userId = undefined;
    }

    async createRoom(params: Record<string, any>): Promise<void> {
        const {room_id: roomId} = await this.post("r0/createRoom", params);
        this.roomId = roomId;

        const keyPackages = [];

        // get all the devices for us and the invitees, and get their
        // key packages
        const users = (params.invite || []).concat([this.userId]);
        this.members = new Set(users);
        this.userDevicesInRoom = new Map();
        const devicesResponse = await this.post(
            "r0/keys/query",
            {
                device_keys: users.reduce((acc, user) => { acc[user] = []; return acc;}, {}),
            },
        );
        this.usersNeedingUpdate.clear();

        const claimDevices: Record<string, Record<string, string>> = {};
        for (const [userId, devices] of Object.entries(devicesResponse.device_keys)) {
            const deviceMap = claimDevices[userId] = {};
            const devicesInRoom: Set<string> = new Set();
            for (const deviceId of Object.keys(devices)) {
                if (userId !== this.userId || deviceId !== this.deviceId) {
                    deviceMap[deviceId] = MLS_INIT_KEY;
                    devicesInRoom.add(deviceId);
                }
            }
            this.userDevicesInRoom.set(userId, devicesInRoom);
        }
        if (Object.keys(claimDevices[this.userId]).length === 0) {
            delete claimDevices[this.userId];
        }
        if (Object.keys(claimDevices).length) {
            const claimResponse = await this.post(
                "r0/keys/claim",
                {
                    one_time_keys: claimDevices,
                },
            );
            for (const [userId, devices] of Object.entries(claimResponse.one_time_keys)) {
                for (const [deviceId, keyMap] of Object.entries(devices)) {
                    for (const key of Object.values(keyMap) as any[]) {
                        // FIXME: check validity
                        const keyPackageEnc = base64Decode(key.key);
                        // eslint-disable-next-line comma-dangle, array-bracket-spacing
                        const [keyPackage, ] = KeyPackage.decode(keyPackageEnc, 0);
                        // FIXME: check signature, etc.
                        keyPackages.push([userId, deviceId, keyPackage]);
                    }
                }
            }
        }

        const [group, , welcome] = await Group.createNew(
            ProtocolVersion.Mls10,
            cipherSuite,
            stringToUint8Array(roomId),
            this.credential,
            this.ed25519,
            keyPackages.map(([, , keyPackage]) => keyPackage),
        );
        this.group = group;
        this.gotSync = true;
        this.notifyWaiters();

        // send welcome messages
        const encodedWelcome = tlspl.encode([welcome.encoder]);
        const payload = {
            welcome: base64Encode(encodedWelcome),
        };
        const messages: Record<string, Record<string, object>> = {};
        // eslint-disable-next-line comma-dangle, array-bracket-spacing
        for (const [userId, deviceId, ] of keyPackages) {
            if (!(userId in messages)) {
                messages[userId] = {};
            }
            messages[userId][deviceId] = payload;
        }
        await this.put(`r0/sendToDevice/org.matrix.msc2883.mls.v0.welcome/${this.txnId++}`, {messages});

        // FIXME: send commit message?
    }

    async leave(): Promise<void> {
        if (this.roomId) {
            await this.post(`r0/rooms/${this.roomId}/leave`);
            await this.post(`r0/rooms/${this.roomId}/forget`);
            this.roomId = undefined;
            delete this.group;
        }
    }

    // Wait until we've 1) joined a room, 2) gotten a Welcome message so we have
    // the MLS group, 3) gotten the membership list
    // Will also be triggered when we create a group.
    waitForGroup(waiter: () => any) {
        if (this.group && this.roomId && this.gotSync) {
            waiter();
        } else {
            this.groupWaiters.push(waiter);
        }
    }

    private notifyWaiters() {
        if (!this.group || !this.roomId || !this.gotSync) {
            return;
        }
        for (const waiter of this.groupWaiters) {
            try {
                waiter();
            } catch (e) {}
        }
        this.groupWaiters = [];
    }

    startSync(onSync: SyncHandler, sentEventHandler: SentEventHandler): void {
        this.sentEventHandler = sentEventHandler;
        this.sync = true;
        this.doSync(onSync);
    }

    private async doSync(onSync: SyncHandler): Promise<void> {
        while (this.sync) {
            // FIXME: make initial sync have a large limit
            const since = this.since ? `&since=${this.since}` : ""
            const result = await this.get(
                `r0/sync?timeout=30000${since}`,
            );
            this.since = result.next_batch;
            let events = [];

            const toDevice = (result.to_device && result.to_device.events) || [];
            for (const event of toDevice) {
                if (event.type === "org.matrix.msc2883.mls.v0.welcome") {
                    try {
                        // eslint-disable-next-line comma-dangle, array-bracket-spacing
                        const [welcome, ] = Welcome.decode(
                            base64Decode(event.content.welcome),
                            0,
                        );
                        const [keyId, group] = await Group.createFromWelcome(
                            welcome, this.oneTimeKeys,
                        );

                        const [epoch, sender] = [...group.extremities.values()][0];
                        event._epoch = epoch;
                        event._sender = uint8ArrayToString(sender).split("|", 2);

                        console.log(
                            this.userId, this.deviceId, "got welcome",
                            epoch, event._sender, event.content.resolves,
                        );

                        this.epochMap.set(epoch + "|" + uint8ArrayToString(sender), event.content.event_id);

                        delete this.oneTimeKeys[keyId];
                        // FIXME: send more otks if needed
                        if (this.group && event.content.resolves) {
                            this.group.addGroupStates(group, event.content.resolves.map(
                                ([epoch, [user, device]]) => [epoch, makeIdentity(user, device)],
                            ));
                        } else {
                            this.group = group;
                            this.notifyWaiters();
                        }
                    } catch (e) {
                        console.error("Could not decrypt welcome message", e);
                    }
                }
            }

            if (result.rooms && result.rooms.join) {
                if (this.roomId) {
                    if (result.rooms.join[this.roomId]) {
                        const room = result.rooms.join[this.roomId];
                        events = ((room.state && room.state.events) || [])
                            .concat((room.timeline && room.timeline.events) || []);
                    }
                } else if (Object.entries(result.rooms.join).length) {
                    const [roomId, room]: [string, any] =
                        Object.entries(result.rooms.join)[0];
                    this.roomId = roomId;
                    events = ((room.state && room.state.events) || [])
                        .concat((room.timeline && room.timeline.events) || []);
                    this.members = new Set([this.userId]);
                    this.userDevicesInRoom = new Map();
                }
                this.notifyWaiters();
            }
            if (result.rooms && result.rooms.leave && this.roomId
                && this.roomId in result.rooms.leave) {
                const room = result.rooms.leave[this.roomId]
                events = ((room.state && room.state.events) || [])
                    .concat((room.timeline && room.timeline.events) || []);
            }

            for (const event of events) {
                if (event.type === "m.room.member" && "state_key" in event) {
                    switch (event.content.membership) {
                        case "join":
                            if (!this.members.has(event.state_key)) {
                                this.members.add(event.state_key);
                                this.usersNeedingUpdate.add(event.state_key)
                            }
                            break;
                        case "leave":
                            this.members.delete(event.state_key);
                            this.usersNeedingUpdate.delete(event.state_key)
                            break;
                        case "invite":
                            if (!this.members.has(event.state_key)) {
                                this.members.add(event.state_key);
                                this.usersNeedingUpdate.add(event.state_key)
                            }
                            break;
                    }
                }
            }

            if (result.device_lists) {
                for (const user of (result.device_lists.changed || [])) {
                    this.usersNeedingUpdate.add(user);
                }
                for (const user of (result.device_lists.left || [])) {
                    this.usersNeedingUpdate.delete(user);
                }
            }

            // auto-join the first room we're invited to
            if (result.rooms && result.rooms.invite &&
                Object.keys(result.rooms.invite).length && !this.roomId) {
                this.roomId = Object.keys(result.rooms.invite)[0];
                this.members = new Set([this.userId]);
                this.userDevicesInRoom = new Map();
                await this.post(`r0/rooms/${this.roomId}/join`);
                this.notifyWaiters();
            } else if (this.roomId && events.length && !this.gotSync) {
                this.gotSync = true;
                this.notifyWaiters();
            }
            await onSync(events, toDevice);
        }
    }

    async updateTree(forceUpdatePath = false): Promise<string> {
        // figure out what devices are in the room
        for (const userId of Object.keys(this.userDevicesInRoom)) {
            if (!this.members.has(userId)) {
                this.userDevicesInRoom.delete(userId);
            }
        }

        if (this.usersNeedingUpdate.size) {
            const usersToUpdate = [...this.usersNeedingUpdate].filter(x => this.members.has(x));
            const devicesResponse = await this.post(
                "r0/keys/query",
                {
                    device_keys: usersToUpdate.reduce((acc, user) => { acc[user] = []; return acc; }, {}),
                },
            );

            for (const userId of usersToUpdate) {
                this.userDevicesInRoom.delete(userId);
            }

            for (const [userId, devices] of Object.entries(devicesResponse.device_keys)) {
                const devicesInRoom: Set<string> = new Set();
                for (const deviceId of Object.keys(devices)) {
                    if (userId !== this.userId || deviceId !== this.deviceId) {
                        devicesInRoom.add(deviceId);
                    }
                }
                this.userDevicesInRoom.set(userId, devicesInRoom);
            }

            this.usersNeedingUpdate.clear();
        }

        const [baseEpoch, baseSender, updates, resolves, updatePathRequired, members, extraUsers] =
            this.group.prepareCommit();

        const newDevices: [string, string][] = [];
        const leavesRemoved: Set<number> = new Set(members.values());
        leavesRemoved.delete(members.get(makeIdentity(this.userId, this.deviceId)));
        for (const [userId, devicesInRoom] of this.userDevicesInRoom) {
            for (const deviceId of devicesInRoom) {
                const identity = makeIdentity(userId, deviceId);
                if (members.has(identity)) {
                    leavesRemoved.delete(members.get(identity));
                } else {
                    newDevices.push([userId, deviceId]);
                }
            }
        }

        if (!forceUpdatePath && !updatePathRequired
            && newDevices.length === 0 && leavesRemoved.size === 0
            && resolves.length === 0) {
            return;
        }

        const proposals: ProposalWrapper[] = updates.map(update => new ProposalWrapper(update));
        for (const leaf of leavesRemoved) {
            const remove = new Remove(leaf);
            proposals.push(new ProposalWrapper(remove));
        }

        const claimDevices: Record<string, Record<string, string>> = {};
        let needsClaim = false;
        for (const [userId, deviceId] of newDevices) {
            const identity = makeIdentity(userId, deviceId);
            if (extraUsers.has(identity)) {
                const add = new Add(extraUsers.get(identity));
                proposals.push(new ProposalWrapper(add));
            } else {
                needsClaim = true;
                if (!(userId in claimDevices)) {
                    claimDevices[userId] = {};
                }
                claimDevices[userId][deviceId] = MLS_INIT_KEY;
            }
        }
        if (needsClaim) {
            const claimResponse = await this.post(
                "r0/keys/claim",
                {
                    one_time_keys: claimDevices,
                },
            );
            for (const devices of Object.values(claimResponse.one_time_keys)) {
                for (const keyMap of Object.values(devices)) {
                    for (const key of Object.values(keyMap) as any[]) {
                        // FIXME: check validity
                        const keyPackageEnc = base64Decode(key.key);
                        // eslint-disable-next-line comma-dangle, array-bracket-spacing
                        const [keyPackage, ] = KeyPackage.decode(keyPackageEnc, 0);
                        // FIXME: check signature, etc.
                        const add = new Add(keyPackage);
                        proposals.push(new ProposalWrapper(add));
                    }
                }
            }
        }

        // eslint-disable-next-line comma-dangle, array-bracket-spacing
        const [mlsCiphertext, mlsPlaintext, welcome, addPositions] =
            await this.group.commit(
                proposals, this.credential, this.ed25519,
                baseEpoch, baseSender, updatePathRequired || forceUpdatePath,
            );

        const [[currEpoch, currEpochSender]] = this.group.extremities.values();

        const resolves2 = resolves.map(
            ([epoch, sender]) => [epoch, uint8ArrayToString(sender).split("|", 2)],
        );

        const encodedMlsCiphertext = tlspl.encode([mlsCiphertext.encoder]);
        const event = {
            type: "m.room.encrypted",
            sender: this.userId,
            room_id: this.roomId,
            content: {
                algorithm: MLS_ALGORITHM,
                ciphertext: base64Encode(encodedMlsCiphertext),
                epoch_sender: uint8ArrayToString(baseSender).split("|", 2),
                resolves: resolves2,
                commit: this.epochMap.get(baseEpoch + "|" + uint8ArrayToString(baseSender)),
            },
        }

        if (this.opts.sendDelay) {
            await new Promise(resolve => {
                setTimeout(resolve, this.opts.sendDelay * Math.random());
            });
        }

        const eventId = await this.send(event, commitToEvent(mlsPlaintext.content as Commit));
        this.epochMap.set(`${currEpoch}|${this.userId}|${this.deviceId}`, eventId);

        // send welcome messages
        const encodedWelcome = tlspl.encode([welcome.encoder]);
        const payload = {
            welcome: base64Encode(encodedWelcome),
            resolves: [[baseEpoch, uint8ArrayToString(baseSender).split("|", 2)]].concat(resolves2),
            event_id: eventId,
        };

        const messages: Record<string, Record<string, object>> = {};
        const keyPackages = this.group.getState(currEpoch, currEpochSender).ratchetTreeView.keyPackages;
        for (const pos of addPositions) {
            const identity = keyPackages[pos].credential.identity;
            const [userId, deviceId] = uint8ArrayToString(identity).split("|", 2);
            if (!(userId in messages)) {
                messages[userId] = {};
            }
            messages[userId][deviceId] = payload;
        }
        await this.put(`r0/sendToDevice/org.matrix.msc2883.mls.v0.welcome/${this.txnId++}`, {messages});

        return eventId;
    }

    async encryptMessage(event: Record<string, any>): Promise<{type: string, content: any, [props: string]: any}> {
        await this.updateTree();

        event.sender = this.userId;
        event.room_id = this.roomId;
        const [mlsCiphertext, epoch, epochSender] = await this.group.encrypt(
            Buffer.from(JSON.stringify(event), "utf8"),
            new Uint8Array(), // FIXME: authenticatedData
            this.ed25519,
        );
        const encodedMlsCiphertext = tlspl.encode([mlsCiphertext.encoder]);

        return {
            type: "m.room.encrypted",
            sender: this.userId,
            room_id: this.roomId,
            content: {
                algorithm: MLS_ALGORITHM,
                ciphertext: base64Encode(encodedMlsCiphertext),
                epoch_sender: uint8ArrayToString(epochSender).split("|", 2),
                commit: this.epochMap.get(epoch + "|" + uint8ArrayToString(epochSender)),
            },
        };
    }

    async send(event: {type: string, content: any, [props: string]: any}, decrypted: any): Promise<string> {
        if (this.roomId) {
            const txnId = this.txnId++ + "";
            this.sentEventHandler(event, decrypted, txnId);
            const res = await this.put(
                `r0/rooms/${this.roomId}/send/${event.type}/${txnId}`,
                event.content,
            );
            return res.event_id;
        }
    }

    async getAndDecryptCommit(eventId: string): Promise<any> {
        const event = await this.get(`r0/rooms/${this.roomId}/event/${eventId}`)
        await this.decryptMessage(event);
    }

    async decryptMessage(event: Record<string, any>): Promise<any> {
        if (!this.group) {
            console.log(this.userId, this.deviceId, event.event_id);
            console.log("Unable to decrypt: no group state yet");
            return;
        }
        // FIXME: assert that content.algorithm is right
        const epochSender = makeIdentity(event.content.epoch_sender[0], event.content.epoch_sender[1]);
        // eslint-disable-next-line comma-dangle, array-bracket-spacing
        const [decodedMlsCiphertext, ] = MLSCiphertext.decode(
            base64Decode(event.content.ciphertext), 0,
        );
        let decryptedMlsPlaintext: MLSPlaintext;
        try {
            decryptedMlsPlaintext = await this.group.decrypt(
                decodedMlsCiphertext, epochSender,
            );
        } catch (e) {
            if (decodedMlsCiphertext.epoch === 1) {
                // encrypted using the initial epoch, which was only sent as
                // welcome messages and not as a room message
                throw e;
            }
            if (event.content.commit === undefined) {
                console.error("prev commit is undefined");
                throw e;
            }
            let promise = this.pending.get(event.content.commit);
            if (promise === undefined) {
                promise = this.getAndDecryptCommit(event.content.commit);
                this.pending.set(event.content.commit, promise);
            }
            await promise;
            decryptedMlsPlaintext = await this.group.decrypt(
                decodedMlsCiphertext, epochSender,
            );
        }
        const senderNode = decryptedMlsPlaintext.sender.sender;
        const senderIdentity = this.group.getState(decodedMlsCiphertext.epoch, epochSender)
            .ratchetTreeView.keyPackages[senderNode].credential.identity;
        event._sender = uint8ArrayToString(senderIdentity).split("|", 2);
        const epochId = decryptedMlsPlaintext.epoch + "|"
            + event.content.epoch_sender[0] + "|"
            + event.content.epoch_sender[1];
        const prevCommit = this.epochMap.get(epochId);
        if (prevCommit !== event.content.commit) {
            console.error(`commit event doesn't match (${event.content.commit},${prevCommit})`);
            return;
        }
        if (decryptedMlsPlaintext.content instanceof Uint8Array) {
            const content = decryptedMlsPlaintext.content;
            return JSON.parse(
                Buffer.from(content.buffer, content.byteOffset, content.length)
                    .toString(),
            );
        } else if (decryptedMlsPlaintext.content instanceof Commit) {
            if (decryptedMlsPlaintext.sender.sender !==
                this.group.getState(decodedMlsCiphertext.epoch, epochSender).leafNum) {
                this.epochMap.set(
                    decryptedMlsPlaintext.epoch + 1 + "|" + uint8ArrayToString(senderIdentity),
                    event.event_id,
                );

                const resolves = event.content.resolves.map(
                    ([epoch, [user, device]]) => [epoch, makeIdentity(user, device)],
                );

                await this.group.applyCommit(
                    decryptedMlsPlaintext, epochSender, resolves,
                );
            }
            return commitToEvent(decryptedMlsPlaintext.content);
        } else {
            throw new Error("Cannot handle proposals yet");
        }
    }
}

function commitToEvent(commit: Commit): Record<string, any> {
    const proposals = [];
    const ev = {
        type: "[MLS Commit]",
        content: {
            proposals: proposals,
            updatePathIncluded: !!commit.updatePath,
        },
    };

    for (const proposalOrRef of commit.proposals) {
        if (proposalOrRef instanceof ProposalWrapper) {
            const proposal = proposalOrRef.proposal;
            if (proposal instanceof Add) {
                const identity = uint8ArrayToString(proposal.keyPackage.credential.identity);
                proposals.push(`Add ${identity}`);
            } else if (proposal instanceof Update) {
                const identity = uint8ArrayToString(proposal.keyPackage.credential.identity);
                proposals.push(`Update ${identity}`);
            } else if (proposal instanceof Remove) {
                proposals.push(`Remove ${proposal.removed}`);
            } else {
                proposals.push("Error: Unknown proposal type");
            }
        } else if (proposalOrRef instanceof Reference) {
            proposals.push(`Proposal ref: ${uint8ArrayToHex(proposalOrRef.hash)}`);
        } else {
            proposals.push("Error: Unknown");
        }
    }
    return ev;
}
