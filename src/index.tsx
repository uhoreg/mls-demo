import React, { useEffect, useState, useReducer, useRef } from "react";
import ReactDOM from "react-dom";
import ReactModal from 'react-modal';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import 'react-tabs/style/react-tabs.css';
import './index.css';
import {Client, MLS_ALGORITHM} from "./client";
import * as tlspl from "mls/lib/tlspl";
import {MLSCiphertext} from "mls/lib/message";
import {base64Decode} from "./util";

function App() {
    const [client, setClient] = useState(null);

    if (client) {
        const logout = () => {
            setClient(null);
        };
        return <LoggedIn client={client} onLogout={logout} />;
    } else {
        const login = async (e) => {
            e.preventDefault();
            const elements = e.target.elements;
            const username = elements.username.value;
            const password = elements.password.value;
            const client = new Client("http://localhost:8008");
            await client.login(username, password);
            setClient(client);
        };
        return (
                <Login login={login} />
        );
    }
}

function Login({login}: {login: (event) => void}) {
    return (
            <form onSubmit={(e) => login(e)}>
              <div>
                <label htmlFor="username">Username: </label>
                <input type="text" name="username" id="username" />
              </div>
              <div>
                <label htmlFor="password">Password: </label>
                <input type="password" name="password" id="password" />
              </div>
              <div>
                <input type="submit" value="Log in" />
              </div>
            </form>
    );
}

function LoggedIn({client, onLogout}: {client: any, onLogout: () => void}) {
    const [eventToShow, setEventToShow] = useState(null);
    const showSource = (event, decrypted) => {
        setEventToShow({event, decrypted});
    };

    const {current: outgoing} = useRef({});

    const [events, updateEvents] = useReducer(
        (events, op) => {
            switch (op.op) {
                case "add":
                    return events.concat(op.events);
                case "sent":
                    {
                        const o = outgoing[op.txnId];
                        if (o) {
                            const newEvents = [...events];
                            newEvents[o.pos] =
                                <RoomEvent event={o.event}
                                    decrypted={o.decrypted}
                                    showSource={showSource} />;
                            return newEvents;
                        } else {
                            return events;
                        }
                    }
                case "addOutgoing":
                    {
                        op.event.unsigned = {transaction_id: op.txnId};
                        outgoing[op.txnId] = {
                            event: op.event,
                            decrypted: op.decrypted,
                            pos: events.length,
                        };
                        return events.concat([
                            <RoomEvent event={op.event}
                                decrypted={op.decrypted}
                                showSource={showSource} sending={true} />,
                        ]);
                    }
                default:
                    return events;
            }
        },
        [],
    );

    useEffect(() => {
        const onSync = async (events: any[], toDevice: any[]) => {
            const newEvents = [];
            for (const e of toDevice) {
                newEvents.push(<IncomingToDevice event={e} showSource={showSource} />);
            }
            for (const e of events) {
                // FIXME: filter out anything that was outgoing
                if (e.unsigned && e.unsigned.transaction_id) {
                    if (e.unsigned.transaction_id in outgoing) {
                        updateEvents({op: "sent", txnId: e.unsigned.transaction_id});
                    }
                } else {
                    if (!("state_key" in e) && e.type === "m.room.encrypted") {
                        try {
                            const decrypted = await client.decryptMessage(e);
                            newEvents.push(<RoomEvent event={e} decrypted={decrypted} showSource={showSource} />);
                        } catch (exception) {
                            console.log(exception); // eslint-disable-line no-console
                            newEvents.push(<UTD event={e} exception={exception} showSource={showSource} />);
                            // throw exception;
                        }
                    } else {
                        newEvents.push(<RoomEvent event={e} showSource={showSource} />);
                    }
                }
            }
            if (newEvents.length) {
                updateEvents({op: "add", events: newEvents});
            }
        };
        client.startSync(
            onSync,
            (event: Record<string, any>, decrypted: any, txnId: string) => {
                updateEvents({op: "addOutgoing", event, decrypted, txnId});
            },
        );

        // Technically, we should clean this up.  But the only time "client"
        // will change is when we log out, and the logout function already does
        // the cleanup.
    }, [client, outgoing]);

    const logout = (e) => {
        client.logout();
        onLogout();
    };
    const sendMsg = async (e) => {
        e.preventDefault();
        const msgControl = e.target.elements.msg;
        const msg = msgControl.value;
        msgControl.value = "";
        if (msg[0] === "/") {
            const cmd = msg.split(/\s+/);
            switch (cmd[0]) {
                case "/chat":
                    await client.createRoom({
                        preset: "private_chat",
                        is_direct: true,
                        invite: cmd.slice(1),
                        initial_state: [
                            {
                                type: "m.room.encryption",
                                state_key: "",
                                content: {
                                    algorithm: MLS_ALGORITHM,
                                },
                            },
                        ],
                    });
                    break;

                case "/leave":
                    await client.leave();
                    break;

                case "/update":
                    await client.updateTree(cmd[1] === "force");
                    break;

                default:
                    window.alert("Unknown command");
            }
        } else {
            const decrypted = {
                type: "m.room.message",
                content: {
                    msgtype: "m.text",
                    body: msg,
                },
            };
            const event = await client.encryptMessage(decrypted);
            await client.send(event, decrypted);
        }
    };
    return (
            <div className="LoggedIn">
              <div>
                Logged in as {client.userId}
                <button type="button" onClick={logout}>Log out</button>
              </div>
              <ul>{events}</ul>
              <div>
                <form onSubmit={sendMsg}>
                  <input name="msg"></input>
                </form>
                <ShowSource event={eventToShow} onRequestClose={() => { setEventToShow(null); }} />
              </div>
            </div>
    );
}

function getEpochInfo(event, decrypted) {
    if (event.type === "m.room.encrypted" && event.content.epoch_sender) {
        // eslint-disable-next-line comma-dangle,array-bracket-spacing
        const [[decodedMlsCiphertext], ] = tlspl.decode(
            [MLSCiphertext.decode], base64Decode(event.content.ciphertext),
        );
        const initialEpoch =
            `${decodedMlsCiphertext.epoch}, ${event.content.epoch_sender[0]}, ${event.content.epoch_sender[1]}`;
        if (decrypted && decrypted.type === "[MLS Commit]") {
            if (event.unsigned && event.unsigned.transaction_id) {
                return `${initialEpoch} → ${decodedMlsCiphertext.epoch + 1}, me`;
            } else {
                return `${initialEpoch} → ${decodedMlsCiphertext.epoch + 1}, ${event._sender[0]}, ${event._sender[1]}`;
            }
        } else {
            return initialEpoch;
        }
        // FIXME: also show resulting epoch for Commits?
    } else {
        return "-";
    }
}

function ShowSource({event, onRequestClose}) {
    const visible = !!event;
    return (
            <ReactModal isOpen={visible}
                contentLabel="Event source"
                onRequestClose={onRequestClose}>
              <Tabs>
                <TabList>
                  <Tab>Event Source</Tab>
                  <Tab disabled={visible && !event.decrypted}>Decrypted</Tab>
                </TabList>
                <TabPanel>
                  <pre>{event && JSON.stringify(event.event, null, 2)}</pre>
                </TabPanel>
                <TabPanel>
                  <pre>{event && event.decrypted && JSON.stringify(event.decrypted, null, 2)}</pre>
                </TabPanel>
              </Tabs>
            </ReactModal>
    );
}

function IncomingToDevice(
    {event, decrypted, showSource}:
    {event: any, decrypted?: any, showSource: (event, decrypted) => void},
) {
    const encryptionMarker = event.type === "m.room.encrypted" ? "🛡️🖥️" : "🖥️";
    const epochInfo = event._epoch ? `→ ${event._epoch}, ${event._sender[0]}, ${event._sender[1]}` : "-";
    return (
            <li>
              <span className="sender">{event.sender}</span>
              <span className="status">{encryptionMarker}</span>
              <span className="event"><code>{JSON.stringify(event)}</code></span>
              <span className="epoch">{epochInfo}</span>
              <button type="button" onClick={() => showSource(event, decrypted)}>&lt;/&gt;</button>
            </li>
    );
}

function RoomEvent(
    {event, decrypted, sending, showSource}:
    {event: any, decrypted?: any, sending?: boolean, showSource: (event, decrypted) => void},
) {
    const sendingMarker = sending ? "📤" : "";
    const encryptionMarker = event.type === "m.room.encrypted" ? "🛡️" : "";
    const marker = sendingMarker + encryptionMarker;
    if ("state_key" in event) {
        return (
                <li>
                  <span className="sender">{event.sender}</span>
                  <span className="status">{marker}</span>
                  <span className="event">
                    {event.type} {event.state_key} <code>{JSON.stringify(event.content, null, 1)}</code>
                  </span>
                  <span className="epoch">-</span>
                  <button type="button" onClick={() => showSource(event, decrypted)}>&lt;/&gt;</button>
                </li>
        );
    } else {
        const e = decrypted || event;
        if (e.type === "m.room.message") {
            return (
                    <li>
                      <span className="sender">{event.sender}</span>
                      <span className="status">{marker}</span>
                      <span className="event">{e.content.body}</span>
                      <span className="epoch">{getEpochInfo(event, decrypted)}</span>
                      <button type="button" onClick={() => showSource(event, decrypted)}>&lt;/&gt;</button>
                    </li>
            );
        } else {
            return (
                    <li>
                      <span className="sender">{event.sender}</span>
                      <span className="status">{marker}</span>
                      <span className="event">
                        {e.type} message: <code>{JSON.stringify(e.content, null, 1)}</code>
                      </span>
                      <span className="epoch">{getEpochInfo(event, decrypted)}</span>
                      <button type="button" onClick={() => showSource(event, decrypted)}>&lt;/&gt;</button>
                    </li>
            );
        }
    }
}

function UTD(
    {event, exception, showSource}:
    {event: any, exception?: any, showSource: (event, decrypted) => void},
) {
    return (
            <li>
              <span className="sender">{event.sender}</span>
              <span className="status">🛡️⛔</span>
              <span className="event">
                <div>Unable to decrypt: {exception.toString()} </div>
                <pre><code>{JSON.stringify(event.content, null, 1)}</code></pre>
              </span>
              <span className="epoch">-</span>
              <button type="button" onClick={() => showSource(event, undefined)}>&lt;/&gt;</button>
            </li>
    );
}

ReactDOM.render(<App />, document.getElementById('root'));
