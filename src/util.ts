import {Buffer} from "buffer";

export function base64Decode(str: string): Uint8Array {
    return Buffer.from(str, "base64");
}

export function base64Encode(bytes: Uint8Array): string {
    return Buffer.from(bytes.buffer).toString("base64");
}

const hexDigits = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F"];

export function uint8ArrayToHex(buffer: Uint8Array): string {
    let rv = "";
    for (let i = 0; i < buffer.length; i++) {
        rv += hexDigits[buffer[i] >> 4];
        rv += hexDigits[buffer[i] & 0xf];
    }
    return rv;
}

export function uint8ArrayToString(buffer: Uint8Array): string {
    let rv = "";
    for (let i = 0; i < buffer.length; i++) {
        rv += String.fromCharCode(buffer[i]);
    }
    return rv;
}

export function sleep(delay: number): Promise<void> {
    return new Promise(resolve => {
        setTimeout(resolve, delay);
    });
}

type MessageHandler = (message: any) => any;

/** Processes tasks serially.
 *
 * A process handles a sequence of tasks.  Only one task is handled at a time.
 * Different processes operate concurrently.
 *
 * Inspired by Erlang's processes and gen_server.
 */
export class Process {
    private mailbox: [any, (r: any) => void, (e: any) => void][] = [];
    private processing = false;
    private active = true;
    constructor(private messageHandler: MessageHandler, private thisArg?: any) {}
    send(message: any): Promise<any> {
        if (!this.active) return Promise.resolve();
        return new Promise((resolve, reject) => {
            if (this.processing) {
                this.mailbox.push([message, resolve, reject]);
            } else {
                this.processing = true;
                (async () => {
                    // console.log(message);
                    try {
                        resolve(await this.messageHandler.call(this.thisArg, message));
                    } catch (e) {
                        reject(e);
                    }
                    while (this.mailbox.length) {
                        const [message, resolve, reject] = this.mailbox.shift();
                        // console.log(message);
                        try {
                            resolve(await this.messageHandler.call(this.thisArg, message));
                        } catch (e) {
                            reject(e);
                        }
                    }
                    this.processing = false;
                })();
            }
        });
    }
    kill(): void {
        if (!this.active) return;
        this.active = false;
        // we can't stop the current handler, but we can prevent it from processing
        // any more
        this.mailbox.length = 0;
    }
}
