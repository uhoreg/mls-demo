import * as d3 from "d3";
import { io } from "socket.io-client";

const socket = io("http://localhost:3000")

const margin = ({top: 20, right: 20, bottom: 20, left: 250});

function y(idx) {
    return idx * step + step / 2;
}

const step = 16;

type Link = {
    source: Node,
    target: Node,
    base?: boolean,
}

type Node = {
    id: string,
    index?: number,
    parent?: string,
    y?: number,
    sourceLinks? : Link[],
    targetLinks? : Link[],
}

const graph = {nodes: [], links: []};

type ClientForEpoch = {
    user: string,
    epoch: string,
    resolves?: string[],
    failed?: boolean
};
const clientsForEpochs: ClientForEpoch[][] = [];

const nodeIdxById: Map<string, number> = new Map();

const svg = d3.select("#canvas");

svg.append("style").text(`

.hover path {
  stroke: #ccc;
}

.hover text {
  fill: #ccc;
}

.hover g.primary text {
  fill: black;
  font-weight: bold;
}

.hover g.secondary text {
  fill: #333;
}

.hover path.primary {
  stroke: #333;
  stroke-opacity: 1;
}

`);

svg.attr("width", "100%");

const label = svg.append("g")
    .attr("font-family", "sans-serif")
    .attr("font-size", 12)
    .attr("text-anchor", "end");

const path = svg.insert("g", "*")
    .attr("fill", "none")
    .attr("stroke-opacity", 0.6)
    .attr("stroke-width", 1.5);

const clientEpochs = svg.append("g")
    .attr("font-family", "sans-serif")
    .attr("font-size", 12)
    .attr("text-anchor", "middle")
    .attr("pointer-events", "all");

const overlay = svg.append("g")
    .attr("fill", "none")
    .attr("pointer-events", "all");

function arc(d: Link) {
    const y1 = y(nodeIdxById.get(d.source.id));
    const y2 = y(nodeIdxById.get(d.target.id));
    const r = Math.abs(y2 - y1) / 2;
    return `M${margin.left},${y1}A${r},${r} 0,0,${y1 < y2 ? 1 : 0} ${margin.left},${y2}`;
}

function updateTree() {
    svg.attr("height", graph.nodes.length * step);

    label.selectAll("g")
        .data(graph.nodes, (d: Node) => d.id)
        .join(
            enter => {
                const g = enter.append("g");
                g.append("text")
                    .attr("x", -6)
                    .attr("dy", "0.35em")
                    .attr("fill", "#000")
                    .text(d => d.id);
                g.append("circle")
                    .attr("r", 3)
                    .attr("fill", d => d.sourceLinks ? "#000" : "#ccc");
                return g;
            },
        )
        .attr("transform", (d, i) => `translate(${margin.left},${y(i)})`);

    path.selectAll("path")
        .data(graph.links, (d: Link) => d.source.id + "|" + d.target.id)
        .join("path")
        .classed("base", d => d.base)
        .attr("stroke", d => d.base ? "#000" : "#aaa")
        .attr("d", arc);

    overlay.selectAll("rect")
        .data(graph.nodes, (d: Node) => d.id)
        .join("rect")
        .attr("width", 80)
        .attr("height", step)
        .attr("y", (d, i) => i * step)
        .attr("x", margin.left - 40)
        .on("mouseover", (e, d) => {
            const l = label.selectAll("g")
                .data(graph.nodes, (d: Node) => d.id);
            const p = path.selectAll("path")
                .data(graph.links, (d: Link) => d.source.id + "|" + d.target.id);
            svg.classed("hover", true);
            l.classed("primary", (n: Node) => n.id === d.id);
            l.classed(
                "secondary",
                (n: Node) => {
                    return n.sourceLinks
                        && (n.sourceLinks.some(l => l.target.id === d.id)
                            || n.targetLinks.some(l => l.source.id === d.id));
                },
            );
            p.classed("primary", (l: Link) => l.source.id === d.id || l.target.id === d.id)
                .filter(".primary").raise();
        })
        .on("mouseout", d => {
            const l = label.selectAll("g");
            const p = path.selectAll("path");
            svg.classed("hover", false);
            l.classed("primary", false);
            l.classed("secondary", false);
            p.classed("primary", false).order();
        });
}

function updateEpochs() {
    clientEpochs.selectAll("g")
        .data(clientsForEpochs)
        .join("g")
        .attr("transform", (d, i) => `translate(${margin.left - 40},${y(i)})`)
        .selectAll("text")
        .data(d => d as any || [])
        .join("text")
        .attr("x", (d, i) => -step * i)
        .attr("dy", "0.35em")
        .attr("fill", (d: ClientForEpoch) => d.failed ? "#ff0000" : "#000")
        .text((d: ClientForEpoch) => d.user);
}

socket.on("epoch", (data) => {
    nodeIdxById.set(data.id, graph.nodes.length);
    data.index = graph.nodes.length;
    data.y = graph.nodes.length * step + step / 2;
    data.sourceLinks = [];
    data.targetLinks = [];
    graph.nodes.push(data);
    if (data.base) {
        const baseNode = graph.nodes[nodeIdxById.get(data.base)];
        const link = {
            source: data,
            target: baseNode,
            base: true,
        };
        graph.links.push(link);
        data.sourceLinks.push(link);
        baseNode.targetLinks.push(link);
    }
    if (data.resolves) {
        data.resolves.forEach((parent) => {
            const target = graph.nodes[nodeIdxById.get(parent)];
            const link = {
                source: data,
                target: target,
            };
            graph.links.push(link);
            data.sourceLinks.push(link);
            target.targetLinks.push(link);
        });
    }
    updateTree();
    processClientUpdate({
        user: data.user,
        epoch: data.id,
        resolved: data.base ? [data.base].concat(data.resolves || []) : undefined,
    });
});

function processClientUpdate(data) {
    // data = {user, resolved, epoch}
    const epochIndex = nodeIdxById.get(data.epoch);
    if (clientsForEpochs[epochIndex]) {
        clientsForEpochs[epochIndex].push(data);
    } else {
        clientsForEpochs[epochIndex] = [data];
    }
    for (const resolvedEpoch of data.resolved || []) {
        const epochIndex = nodeIdxById.get(resolvedEpoch);
        const epochArr = clientsForEpochs[epochIndex];
        if (epochArr) {
            clientsForEpochs[epochIndex] = epochArr.filter(x => x.user !== data.user);
        }
    }
    updateEpochs();
}

socket.on("decrypted", processClientUpdate);

function failedDecryption(data) {
    // data = {user, epoch}
    data.failed = true;
    const epochIndex = nodeIdxById.get(data.epoch);
    if (clientsForEpochs[epochIndex]) {
        clientsForEpochs[epochIndex].push(data);
    } else {
        clientsForEpochs[epochIndex] = [data];
    }
    updateEpochs();
}

socket.on("failedDecryption", failedDecryption);

function left(data) {
    // data = {user}
    data.id = `${data.user} left`;
    data.index = graph.nodes.length;
    data.y = graph.nodes.length * step + step / 2;
    graph.nodes.push(data);
    updateTree();
}

socket.on("left", left);

function joined(data) {
    // data = {user}
    data.id = `${data.user} joined`;
    data.index = graph.nodes.length;
    data.y = graph.nodes.length * step + step / 2;
    graph.nodes.push(data);
    updateTree();
}

socket.on("joined", joined);
